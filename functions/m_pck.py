import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


#from m_json import* #importing all functions from m_json.py
# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib

    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))

from functions import m_json


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    # TODO: Print serials and temperature values of all connected sensors.
    # HINT: Use the W1ThermSensor Library.
    # This line of code throws an exception. This is just to make sure you can see
    # all the code you need to refine. If you already know how to implement the program
    # or have done so, then you can safely delete the three lines of code below, as well
    # as this comment.
    
    from w1thermsensor import W1ThermSensor
    
    for sensor in W1ThermSensor.get_available_sensors():
        print("Sensor %s has temperature %.2f" % (sensor.id,sensor.get_temperature()))
    

    # DONE #


def get_meas_data_calorimetry(metadata: dict) -> dict:
    #get_meas_data_calorimetry(add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets',get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')))
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict: A dictionary with sensor uuid as keys, and corresponding lists of temperatures and timestamps.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    start = time.time()
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
    ]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
               
               uuid=metadata["sensor"]["values"][i] 		# get uuids
               temperature = sensor.get_temperature()  		#measure temperature
               timestamp = time.time() - start 				#start timer
               
               data[uuid][0].append(temperature) 			#append temperatures and timestamps to correct uuid
               data[uuid][1].append(timestamp)
               
               
               time.sleep(1)								#wait one second           

                # DONE #
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.
    # Set attributes for the H5 file based on the datasheets.
    
    from datetime import datetime										# needed to access current time
    now = datetime.now()
    
    f.attrs["created"] = now.strftime('%d-%m-%Y, %H:%M:%S')				# deklaring all needed attributes
    f.attrs["experiment"] = m_json.get_json_entry(json_folder,metadata["group_info"]["values"][0], ["group","experiment"])
    f.attrs["group_number"] = m_json.get_json_entry(json_folder,metadata["group_info"]["values"][0], ["group","number"])
    f.attrs["authors"] = m_json.get_json_entry(json_folder,metadata["group_info"]["values"][0], ["group","author"])
    
    # I wanted to solve the problem with the for loop below but the line 'f.attrs["created"] = None' that was provided from the git repo caused an error
    #
#error: Traceback (most recent call last):
  #File "<stdin>", line 1, in <module>
  #File "/home/pi/calorimetry_home/functions/m_pck.py", line 189, in logging_calorimetry
  #  f.attrs["created"] = None
  #File "h5py/_objects.pyx", line 54, in h5py._objects.with_phil.wrapper
  #File "h5py/_objects.pyx", line 55, in h5py._objects.with_phil.wrapper
  #File "/home/pi/.local/lib/python3.9/site-packages/h5py/_hl/attrs.py", line 103, in __setitem__
  #  self.create(name, data=value)
  #File "/home/pi/.local/lib/python3.9/site-packages/h5py/_hl/attrs.py", line 180, in create
  #  htype = h5t.py_create(original_dtype, logical=True)
  #File "h5py/h5t.pyx", line 1663, in h5py.h5t.py_create
  #File "h5py/h5t.pyx", line 1687, in h5py.h5t.py_create
  #File "h5py/h5t.pyx", line 1747, in h5py.h5t.py_create
  #TypeError: Object dtype dtype('O') has no native HDF5 equivalent
    
    #for i in ["created", "experiment", "group_number", "authors"]: 				#loop that deklares all attributes where there is None till now
    #    if f.attrs[i] is None:
    #        
    #        if i == "created":														#if Attribute created is empty its filled with the current time
    #            f.attrs[i]=now.strftime('%d-%m-%Y, %H:%M:%S')
    #        
    #        else:																	#all other attributes are deklared with data from metadata
    #            f.attrs[i]=get_json_entry(json_folder,metadata["group_info"]["values"][0], ["group",i])
            
            
            

    # DONE #

    # TODO: Write data to HDF5.
       
    for i in range(0,len(list(data))): 											# for every sensor a subgroup with attributes name and serials is created 
        grp = grp_raw.create_group(list(data)[i])
        
        grp.attrs["name"]=list(metadata["sensor"]["names"])[i] 					#name and serials are taken from metadata
        grp.attrs["serials"]=list(metadata["sensor"]["serials"])[i]
        
        
        temps=grp.create_dataset("temperatures",data=data[list(data)[i]][0]) 	#in every subgroup the matching measurement data is placed
        time=grp.create_dataset("timestamps",data=data[list(data)[i]][1])
        temps.attrs["unit"]="degree_celcius"									#units are added to datasets
        time.attrs["unit"]="seconds"
    
    

    # DONE #

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    pass
