
from functions import m_json
from functions import m_pck

#path = '/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json' #path to needed setup sepending on what experiment has to be conductet
path = '/home/pi/calorimetry_home/datasheets/setup_newton.json'

metadata = m_json.get_metadata_from_setup(path) # geting metadata from setupfile

metadata=m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets',metadata) #adding the serial numbers to metadata
data=m_pck.get_meas_data_calorimetry(metadata) #measuring data for experiment

# adding data to hdf5 file
#m_pck.logging_calorimetry(data,metadata,'/home/pi/calorimetry_home/data/heat_capacity/try_one','/home/pi/calorimetry_home/datasheets') 
m_pck.logging_calorimetry(data,metadata,'/home/pi/calorimetry_home/data/newton/try_one','/home/pi/calorimetry_home/datasheets') 

#adding archive
#m_json.archiv_json('/home/pi/calorimetry_home', '/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json', '/home/pi/calorimetry_home/data/heat_capacity/try_one/archive') 
m_json.archiv_json('/home/pi/calorimetry_home', '/home/pi/calorimetry_home/datasheets/setup_newton.json', '/home/pi/calorimetry_home/data/newton/try_one/archive')